Admin.controllers :scrapes do

  get :index do
    @scrapes = Scrape.all
    render 'scrapes/index'
  end

  get :new do
    @scrape = Scrape.new
    render 'scrapes/new'
  end

  post :create do
    @scrape = Scrape.new(params[:scrape])
    if @scrape.save
      flash[:notice] = 'Scrape was successfully created.'
      redirect url(:scrapes, :edit, :id => @scrape.id)
    else
      render 'scrapes/new'
    end
  end

  get :edit, :with => :id do
    @scrape = Scrape.get(params[:id])
    render 'scrapes/edit'
  end

  put :update, :with => :id do
    @scrape = Scrape.get(params[:id])
    if @scrape.update(params[:scrape])
      flash[:notice] = 'Scrape was successfully updated.'
      redirect url(:scrapes, :edit, :id => @scrape.id)
    else
      render 'scrapes/edit'
    end
  end

  delete :destroy, :with => :id do
    scrape = Scrape.get(params[:id])
    if scrape.destroy
      flash[:notice] = 'Scrape was successfully destroyed.'
    else
      flash[:error] = 'Unable to destroy Scrape!'
    end
    redirect url(:scrapes, :index)
  end
end
