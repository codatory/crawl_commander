Admin.controllers :domain_policies do

  get :index do
    @domain_policies = DomainPolicy.all
    render 'domain_policies/index'
  end

  get :new do
    @domain_policy = DomainPolicy.new
    render 'domain_policies/new'
  end

  post :create do
    @domain_policy = DomainPolicy.new(params[:domain_policy])
    if @domain_policy.save
      flash[:notice] = 'DomainPolicy was successfully created.'
      redirect url(:domain_policies, :edit, :id => @domain_policy.id)
    else
      render 'domain_policies/new'
    end
  end

  get :edit, :with => :id do
    @domain_policy = DomainPolicy.get(params[:id])
    render 'domain_policies/edit'
  end

  put :update, :with => :id do
    @domain_policy = DomainPolicy.get(params[:id])
    if @domain_policy.update(params[:domain_policy])
      flash[:notice] = 'DomainPolicy was successfully updated.'
      redirect url(:domain_policies, :edit, :id => @domain_policy.id)
    else
      render 'domain_policies/edit'
    end
  end

  delete :destroy, :with => :id do
    domain_policy = DomainPolicy.get(params[:id])
    if domain_policy.destroy
      flash[:notice] = 'DomainPolicy was successfully destroyed.'
    else
      flash[:error] = 'Unable to destroy DomainPolicy!'
    end
    redirect url(:domain_policies, :index)
  end
end
