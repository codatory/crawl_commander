class DomainPolicy
  include DataMapper::Resource
  include DataMapper::Validate

  require 'net/http'

  attr_accessor :other, :disallows, :allows, :delays

  # property <name>, <type>
  property :id, Serial
  property :domain, String
  property :raw, Text
  property :expires, Time

  USER_AGENT  = /CrawlCommanderAgent/

  validates_presence_of :domain
  before :save, :fetch_body

  def self.remove_expired
    all(:expires => "< #{Time.now}").destroy
  end

  def expired?
    expires < Time.now
  end

  def fetch_body
    unless raw.present? && !expired?
      data = Net::HTTP.get_response(URI.parse("http://#{domain}/robots.txt"))

      if data.response.is_a?(Net::HTTPSuccess) && data.content_type == 'text/plain'
        self.raw      = data.body
        self.expires  = data.get_fields('expires') || Time.now + 60*60*24
      else
        self.raw      = 'User-agent: *\nAllow: /\n'
        self.expires  = Time.now + 60*60*6
      end
    end
  end

  def parse
    if expired?
      fetch_body
      self.save
    end

    return true if @parsed

    @other      = {}
    @disallows  = {}
    @allows     = {}
    @delays     = {}
    @sitemaps   = []
    @parsed     = true

    agent = '/.*/'
    raw.each_line do |line|
      next if line =~ /^\s*(#.*|$)/
      arr = line.split(":")
      key = arr.shift
      value = arr.join(":").strip
      value.strip!
      case key
      when "User-agent"
        agent = value
      when "Allow"
        @allows[agent] ||= []
        @allows[agent] << to_regex(value)
      when "Disallow"
        @disallows[agent] ||= []
        @disallows[agent] << to_regex(value)
      when "Crawl-delay"
        @delays[agent] = value.to_i
      when "Sitemap"
        @sitemaps << value
      else
        @other[key] ||= []
        @other[key] << value
      end
    end
  end

  def delay
    parse unless @parsed
    @delays['/.*/'] || 5
  end

  def sitemaps
    parse unless @parsed
    @sitemaps
  end

  def allowed?(path)
    parse unless @parsed
    allowed = true

    path = path.request_uri if path.is_a?(URI)

    @disallows.each do |key, value|
      if USER_AGENT =~ key || key =~ /.*/
        value.each do |rule|
          if path =~ rule
            allowed = false
          end
        end
      end
    end

    @allows.each do |key, value|
      unless allowed
        if USER_AGENT =~ key || key =~ /.*/
          value.each do |rule|
            if path =~ rule
              allowed = true
            end
          end
        end
      end
    end

    return allowed
  end

  private
    def to_regex(pattern)
      return /should-not-match-anything-123456789/ if pattern.strip.empty?
      return "/.*/" if pattern == "*"
      pattern = Regexp.escape(pattern)
      pattern.gsub!(Regexp.escape("*"), ".*")
      Regexp.compile("^#{pattern}")
    end
end
