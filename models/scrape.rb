class Scrape
  include DataMapper::Resource

  QUEUE = Beanstalk::Pool.new(['127.0.0.1:11300'])

  # property <name>, <type>
  property :id, Serial
  property :job_id, Integer
  property :url, String
  property :title, String
  property :description, Text
  property :meta_keywords, Text
  property :body, Text
  property :started_at, Time
  property :finished_at, Time
  property :link_level, Integer

  validates_uniqueness_of :url, :scope => :job_id
  validates_format_of :url, :with => URI::regexp(%w(http https))
  validates_with_method :url_is_allowed?

  belongs_to :job

  def uri
    @uri ||= URI.parse(url)
  end

  def domain_policy
    @domain_policy ||= DomainPolicy.first_or_create(:domain => uri.host)
  end

  def priority
    if job.priority.zero? || job.priority.nil?
      1
    else
      job.priority
    end
  end

  def beanstalk_priority
    (65536 / priority)
  end

  def enqeue
    scrape_data = JSON.generate({
      :id   => id,
      :url  => url
    })

    QUEUE.put(scrape_data, beanstalk_priority, domain_policy.delay)
    self.update(:started_at => Time.now)
  end

  private

    def url_is_allowed?
      domain_policy.allowed?(uri) rescue true
    end
end
