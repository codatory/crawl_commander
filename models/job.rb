class Job
  include DataMapper::Resource

  # property <name>, <type>
  property :id, Serial
  property :url, String
  property :started_at, Time
  property :finished_at, Time
  property :link_level, Integer
  property :priority, Integer

  after :create, :build_first_scrape

  has n, :scrape

  def self.unstarted
  	all(:started_at => nil)
  end

  def add_links(links,link_level)
  	new_link_level = link_level -1
  	links.to_a.each do |link|
     link_uri = URI.parse(link) rescue nil
  	 Scrape.first_or_create(:job_id => id, :url => link_uri) if link_uri.present?
  	end
  end

  private
  	def build_first_scrape
  	  if scrape.count.zero?
  	  	Scrape.create(
  	  	  :job_id 		=> id,
  	  	  :url    		=> url,
  	  	  :link_level 	=> link_level
  	  	).enqeue
  	  end
  	end
end
