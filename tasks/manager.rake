task :manager => :environment do

  IN_QUEUE  = Beanstalk::Pool.new(['127.0.0.1:11301'])
  OUT_QUEUE = Beanstalk::Pool.new(['127.0.0.1:11300'])

  puts 'Starting manager...'

  loop do
    write_job = IN_QUEUE.reserve(10) rescue nil

    Scrape.transaction do
      if write_job.present?
        print '  Writing...'
        data = JSON.parse(write_job.body)
        scrape = Scrape.first(data['id']).first
        next_scrape = nil
        print '.'
        if scrape.present? && scrape.is_a?(Scrape)
          print '.'
          scrape.update(
            :url            => data['url'],
            :title          => data['title'],
            :description    => data['description'],
            :meta_keywords  => data['meta_keywords'],
            :body           => Sanitize.clean(data['html'])[0..1024],
            :finished_at    => Time.parse(data['finished_at'])
          )
          print '.'
          scrape.job.add_links(data['links'], scrape.link_level)
          print '.'
          next_scrape = scrape.job.scrape.first(:started_at => nil)
          print '.'
          scrape.job.update(:finished_at => Time.now) unless next_scrape.present?
          write_job.delete
          print '.'
        end

        if next_scrape.present?
          next_scrape.enqeue
          print '.'
        end
        puts 'done!'
      end
    end

    puts 'Still Alive...'
  end
end
