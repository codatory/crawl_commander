task :scraper => :environment do

    IN_QUEUE  = Beanstalk::Pool.new(['127.0.0.1:11300'])
    OUT_QUEUE = Beanstalk::Pool.new(['127.0.0.1:11301'])

    puts "Started scraping..."

    pool = ThreadPool.new
    pool.resize(20)

    loop do
      job = IN_QUEUE.reserve(10) rescue nil
      if job.present?
        pool.process(job) do |task|
          data = JSON.parse(task.body)

          puts " => #{data['url']}"
          inspector = MetaInspector.new(data['url'])
          page      = inspector.to_hash rescue {}
          body      = inspector.document rescue nil

          if page.present?
            response = {
              :id             => data['id'],
              :url            => page['url'],
              :title          => page['title'],
              :description    => page['meta_description'],
              :meta_keywords  => page['meta_keywords'],
              :html           => inspector.document,
              :links          => page['absolute_links'],
              :finished_at    => Time.now
            }

            OUT_QUEUE.put(JSON.generate(response))
          end
          task.delete
        end
      end

      puts " Inbound Queue (scrapes) => #{IN_QUEUE.stats['current-jobs-ready']}"
      puts " Outbound Queue (writes) => #{OUT_QUEUE.stats['current-jobs-ready']}"
      puts ''

      sleep 1
  	end

end
